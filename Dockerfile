FROM php:7.4-fpm-alpine
FROM php:7.4-apache

LABEL maintainer="Saberes Saber <saberesemrede.saber@gmail.com>"
LABEL description="Moodle do projeto Saberes em Rede"
LABEL version="1.0.0"

ENV DEBIAN_FRONTEND=noninteractive

COPY ./000-default.conf /etc/apache2/sites-available/

RUN apt-get update && apt-get install -y \
    libzip-dev \
    zip && \
    docker-php-ext-install zip

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev && \
    docker-php-ext-configure gd --with-jpeg --with-freetype && \
    docker-php-ext-install -j$(nproc) gd

RUN apt-get update && apt-get install -y \
    libicu-dev && \
    docker-php-ext-configure intl && \
    docker-php-ext-install intl


RUN apt-get update && apt-get install -y \
    cron \
    g++ \
    gettext \
    libicu-dev \
    openssl \
    libc-client-dev \
    libkrb5-dev \
    libxml2-dev \
    libfreetype6-dev \
    libgd-dev \
    libmcrypt-dev \
    bzip2 \
    libbz2-dev \
    libtidy-dev \
    libcurl4-openssl-dev \
    libz-dev \
    libmemcached-dev \
    libxslt-dev \
    libzip-dev

RUN a2enmod rewrite

RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable mysqli


RUN apt-get update && apt-get install -y \
    postfix \
    vim && \
    sed -i 's/128M/513M/g' /usr/local/etc/php/php.ini-production && \
    sed -i 's/post_max_size\ =\ 8M/post_max_size\ =\ 200M/g' /usr/local/etc/php/php.ini-production && \
    sed -i 's/2M/200M/g' /usr/local/etc/php/php.ini-production && \
    sed -i 's/max_file_uploads\ =\ 20/max_file_uploads\ =\ 100/g' /usr/local/etc/php/php.ini-production && \
    sed -i 's/;max_input_vars\ =\ 1000/max_input_vars\ =\ 5000/g' /usr/local/etc/php/php.ini-production

RUN mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

COPY moodle/ /var/www/html/

COPY run.sh /run.sh

RUN chmod +x /run.sh

